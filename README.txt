CS 360 - Lab 7
Author: Ben Simpson

Git repository: https://bitbucket.org/benssimpson/cs360-lab7

URL of jquery script (weather.html page): http://ec2-54-88-82-132.compute-1.amazonaws.com:3000/

URL of REST server: http://ec2-54-88-82-132.compute-1.amazonaws.com:3000/getcity

All files for the project are in this git repository.  The main html can be found in public/weather.html.  The main javascript that runs the rest services can be found in routes/index.js.
