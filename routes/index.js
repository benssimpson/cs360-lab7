var express = require('express');
var router = express.Router();
var fs = require('fs');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendFile('weather.html', {root: 'public'});
});
router.get('/getcity', function(req,res) {
	console.log("In getcity");
	var myRe = new RegExp("^" + req.query.q);
          console.log(myRe);
	fs.readFile(__dirname + '/cities.dat.txt',function(err,data) {
            if(err) throw err;
	var cities = data.toString().split("\n");
            var jsonresult = [];
            for(var i = 0; i < cities.length; i++) {
              var result = cities[i].search(myRe); 
              if(result != -1) {
               console.log(cities[i]);
               jsonresult.push({city:cities[i]});
              } 
            }   
        console.log(jsonresult);
	res.status(200).json(jsonresult);	    
          });
});

router.get('/googlemap', function(req,res) {
  console.log("In googlemap");
  var center = req.query.center;
  var mapsrc="https://www.google.com/maps/embed/v1/view?key=AIzaSyCRBZPKUBAz80GtmWyFCfcu1V-v8jephc8&center=";
  mapsrc += center;
  mapsrc += "&zoom=10";
  var maphtml = "<iframe width=450 height=250 style=border:0 src=";
  maphtml += mapsrc;
  maphtml += " allowfullscreen></iframe>";
  res.status(200).end(maphtml);
});

module.exports = router;
